import view.MainView;
import controller.MainController;
import model.Counter;


public class Main {

	public static void main(String[] args) {
		Counter c = new Counter();
		MainController control = new MainController(c);
		MainView view = new MainView(c, control);
		c.addObserver(view);
	}
	
}
