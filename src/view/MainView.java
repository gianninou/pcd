package view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;

import model.Counter;
import controller.MainController;

public class MainView extends JFrame implements Observer{

	private static final long serialVersionUID = 4710543414366048694L;

	private MainController control;
	
	private Counter counter;
	
	private JLabel min;
	private JLabel max;
	private JLabel value;
	
	public MainView(Counter c, final MainController control){
		super("CounterApp");
		 JPanel panel;
		
		 
		
		 JButton plus;
		 JButton minus;
		
		 JMenuBar menuBar;
		JMenu fichier, modifier, intervalle;
		
		JMenuItem quitter, plus1, minus1, defineMin, defineMax;
		
		this.counter = c;
		this.control=control;
		
		panel = new JPanel();
		panel.setLayout(new GridLayout(2, 3));
		
		min = new JLabel(""+c.getMin(),JLabel.CENTER);
		max = new JLabel(""+c.getMax(),JLabel.CENTER);
		value = new JLabel(""+c.getValue(),JLabel.CENTER);
		
		minus = new JButton("-1");
		ActionListener dec = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				control.decrement();
			}
		};
		
		plus = new JButton("+1");
		ActionListener inc = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				control.increment();
			}
		};
		
		
		
		minus.addActionListener(dec);
		plus.addActionListener(inc);
		
		
		panel.add(min);
		panel.add(value);
		panel.add(max);
		
		panel.add(minus);
		panel.add(new JLabel(""));
		panel.add(plus);
		
		
		this.add(panel);
		
		menuBar = new JMenuBar();
		
		fichier = new JMenu("fichier");
		modifier = new JMenu("modifier");
		intervalle = new JMenu("intervalle");
		

		quitter = new JMenuItem("Quitter");
		plus1 = new JMenuItem("+1");
		plus1.addActionListener(inc);
		minus1 = new JMenuItem("-1");
		minus1.addActionListener(dec);
		defineMin = new JMenuItem("Définir valeur minimale");
		defineMax = new JMenuItem("Définir valeur maximale");
		
		ActionListener setMax = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String max = JOptionPane.showInputDialog(null,"Valeur maximale : ", null);
				if(max==null || max.isEmpty()){
					JOptionPane.showMessageDialog(null, "Vueillez entrer un nombre");
				}
				try{
					control.changeMax(Integer.parseInt(max));
				}catch(NumberFormatException nfe){
					JOptionPane.showMessageDialog(null, "Vueillez entrer un nombre");
				}
			}
		};
		
		ActionListener setMin = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String min = JOptionPane.showInputDialog(null,"Valeur minimale : ", null);
				if(min==null || min.isEmpty()){
					JOptionPane.showMessageDialog(null, "Vueillez entrer un nombre");
				}
				try{
					control.changeMin(Integer.parseInt(min));
				}catch(NumberFormatException nfe){
					JOptionPane.showMessageDialog(null, "Vueillez entrer un nombre");
				}
			}
		};
		
		ActionListener quitterAct = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		};
		
		defineMin.addActionListener(setMin);
		defineMax.addActionListener(setMax);
		quitter.addActionListener(quitterAct);
		
		fichier.add(quitter);
		modifier.add(plus1);
		modifier.add(minus1);
		intervalle.add(defineMin);
		intervalle.add(defineMax);
		
		menuBar.add(fichier);
		menuBar.add(modifier);
		menuBar.add(intervalle);
		
		setJMenuBar(menuBar);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(200,200);
		this.setPreferredSize(new Dimension(300, 150));
		this.pack();
		this.setVisible(true);
	}
	
	
	@Override
	public void update(Observable arg0, Object arg1) {
		min.setText(""+counter.getMin());
		max.setText(""+counter.getMax());
		value.setText(""+counter.getValue());
		repaint();
	}

}
