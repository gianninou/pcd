package model;

import java.util.Observable;

public class Counter extends Observable{
	
	private int min;
	private int max;
	private int value;
	
	
	public Counter(int min, int max, int value){
		this.min = min;
		this.max = max;
		this.value = value;
	}
	
	public Counter(){
		this.min = -5;
		this.max = 25;
		this.value = 0;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		if(min<=value){
			this.min = min;
			setChanged();
			notifyObservers();
		}
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		if(max>=value){
			this.max = max;
			setChanged();
			notifyObservers();
		}
	}

	public int getValue() {
		return value;
	}

	public void increment(){
		if(value<max){
			value++;
			setChanged();
			notifyObservers();
		}
	}
	
	public void decrement(){
		if(value>min){
			value--;
			setChanged();
			notifyObservers();
		}
	}
	
	

}
