package controller;

import view.MainView;
import model.Counter;

public class MainController{
	
	
	private Counter counter;
	
	public MainController(Counter c){
		this.counter = c;
	}
	
	public void increment(){
		counter.increment();
	}
	
	public void decrement(){
		counter.decrement();
	}
	
	public void changeMin(int min){
		counter.setMin(min);
	}
	
	public void changeMax(int max){
		counter.setMax(max);
	}
	
}
